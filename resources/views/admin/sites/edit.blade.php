@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Редагування сайту</h2>
    <form action="/admin/sites/{{ $site->site_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <label>Назва сайту</label>
        <input type="text" name="title" value="{{$site->title}}">
        <br/><br/>

        <label>Номер сайту</label>
        <input type="text" name="site_id" value="{{$site->site_id}}">
        <br/><br/>
        <label>Жанри</label>
        <select name="janre">
            @foreach($janre_type as $janre_id => $janre_title)
                <option value="{{ $janre_id }}"
                    {{ ( $janre_id == $site->janre ) ? 'selected' : '' }}>
                    {{ $janre_title }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
