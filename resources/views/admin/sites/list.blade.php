@extends('admin.layout')

@section('content')
<h2>Список сайтів</h2>

<table border="1" style="text-align: center;">
    <th>Назва сайту</th>
    <th>Трафік</th>
    <th>Жанр</th>
    <th>Дія</th>
    @foreach($sites as $site)
        <tr>
            <td>
                <a href="/sites/{{$site->site_id}}">{{$site->title}}</a>
            </td>
            <td>{{$site-> traffic}}</td>
            <td>{{$site-> janre}}</td>

            <td>
                <a href="/admin/sites/{{$site->site_id}}/edit">edit</a>

                <form style="float: right; padding: 0 15px;"
                      action="/admin/sites/{{$site->site_id}}"method="POST">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button>DELETE</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>

@endsection
