@extends('admin.layout')

<style type="text/css">
    label{
        min-width: 150px;
        display: inline-block;
    }
</style>

@section('content')
    <h2>Додати групу</h2>

    <form action="/admin/sites" method="POST">
        {{csrf_field() }}

        <label>Номер сайту</label>
        <input type="text" name="site_id">
        <br/><br/>

        <label>Назва</label>
        <input type="text" name="title">
        <br/><br/>

        <label>Трафік</label>
        <input type="text" name="traffic">
        <br/><br/>

        <label>Кількість відвідувачів</label>
        <input type="text" name="visit">
        <br/><br/>

        <label>Жанр</label>
        <select name="janre">
            @foreach($janre_type as $janre_id => $janre_title)
                <option value="{{$janre_id}}">
                    {{$janre_title}}
                </option>
            @endforeach
        </select>
        <br/><br/>

        <input type="submit" value="Зберегти">
    </form>
