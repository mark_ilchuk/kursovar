<html>
<head>
    <title>Інформаційна система "Google analytics"</title>
    <link href="{{asset('public/css/style.css')}}" rel="stylesheet">
</head>

<style type="text/css">

    .textcol {
        color: #edf2f7;
    }

    .textcol2{
        color: #7952b3;
    }

    .container{
        text-align: center;
        border-style: outset;
        align-content: center;
    }

    #up{
        margin: auto;
        padding: 20px;
        background-color: #7952b3;
    }

    body{
        margin: 0;
        font-family: var;
        font-size: 1rem;
        font-weight: 400;
        line-height: 3;
        color: #212529;
        background-color: #fff;
    }

    #tab{
        margin: auto ;
    }


</style>
<body>
    @yield('page_title'){{--динамічна змінна--}}
    <br/><br/>

    <div class="container">
        Інформація про сайти
        @yield('content'){{--динамічна змінна--}}
    </div>

    <br/><br/>
</body>

</html>
