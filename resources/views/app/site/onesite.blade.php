@extends('app.layouts.layout')

@section('page_title')
    <b id="up">Інформація про сайт <span class="textcol" >{{ $site->title }}</span></b>
@endsection

@section('content')
    <p>Номер сайту - {{$site->site_id}}</p>
    <p>Назва - <span class="textcol2">{{$site->title}}</span></p>
    <p>Відвідуваність сайту за 3 місяці - {{$site->visit}} особи</p>
    <p>Трафік - {{$site->traffic}}</p>

    <a href="/sites/">Вивести всі сайти</a>
@endsection


