@extends('app.layouts.layout')

@section('page_title')
    <b id="up">Список сайтів</b>
@endsection

<?php
    $model_sites = new \App\Models\Sites();
    $janres = $model_sites->getSitesJanre();
?>

@section('content')
    <div class="container">
    <b>Вибір жанру сайта</b>
        <form method="get" action="/sites">
        <select name="janre">
            <option value="0">всі жанри</option>

            @foreach($janres as $janre)
                <option value="{{ $janre->janre}}"
                    {{ ( $janre->janre == $janre_selected ) ? 'selected' : '' }}>
                    {{ $janre->janre}}
                </option>
            @endforeach
        </select>
        <input type="submit" value="Знайти" />
    </form>
    </div>

    <div class="container1">
    <table id="tab" border="1">
        <th>Номер сайту</th>
        <th>Назва</th>
        <th>Трафік</th>
        <th>Жанр</th>

        @foreach ($sites as $site)
            <tr>
                <td>
                    <a href="/sites/{{ $site->site_id }}">
                        {{ $site->site_id}}
                    </a>
                </td>
                <td> {{ $site->title }} </td>
                <td>{{ $site->traffic }}</td>
                <td>{{ $site->janre}}</td>
            </tr>
        @endforeach
    </table>
    </div>
@endsection


