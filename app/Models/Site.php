<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    use HasFactory;

    /*protected $table = "sites";*/

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    protected $primaryKey = 'site_id';

    /**
     * Indicates if the model should be timestaped.
     */
    public $timestamps = false;

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
}
