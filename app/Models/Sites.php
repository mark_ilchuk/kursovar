<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sites extends Model
{

    public $site;
    public $janre;

    public function setJanre($janre)
    {
        $this->janre = $janre;
    }

    public function getJanre()
    {
        return $this->janre;
    }

    public static $janre_type = array(
        "1" => "магазини",
        '2' => "мережі",
        '3' => "фільми"

    );

    public function getSitesJanre(){
            return DB::select('select distinct(janre) from google_an2.sites order by janre');
    }

    public function getSites($janre){

        $query = DB::table('sites');

        $query->select('site_id', 'title', 'visit', 'traffic', 'janre')
            ->orderBy('site_id');

        if ($janre){
            $query->where('janre', '=', $janre);
        }

        $sites = $query->get();
        return $sites;
    }

    public function getSiteByID($site_id){
        if(!$site_id) return null;

        $sites = DB::table('sites')
            ->select('*')//вибираємо усе з таблиці сайти, це можна не писати
            ->where('site_id', $site_id)
            ->get()->first();//повертає один запит

        return $sites;
    }
}







