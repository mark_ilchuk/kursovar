<?php

namespace App\Http\Controllers;

use App\Models\Sites;
use Illuminate\Http\Request;

class SitesController extends Controller
{
    public function index(Request $request){
        $janre = $request->input('janre', null);

        $model_sites = new Sites();

        $sites = $model_sites->getSites($janre);
        return view('app.site.allsite', [
            'sites' => $sites,
            'site_rewards' => Sites::$janre_type,
                'janre_selected'=>$janre
            ]
        );
    }

    public function site($id){
        $model_sites = new Sites();
        $site = $model_sites->getSiteByID($id);

        return view('app.site.onesite')->with('site', $site);
    }
}
