<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\Models\Sites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminSitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = Site::get();
        return view('admin.sites.list', ['sites' => $sites]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sites.add', ['janre_type' => Sites::$janre_type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $site_id = $request->input('site_id');
        $title = $request->input('title');
        $janre = $request->input('janre');
        $visit = $request->input('visit');
        $traffic = $request->input('traffic');


        $site = new Site();
        $site->title = $title;
        $site->janre = Sites::$janre_type[$janre];
        $site->site_id = $site_id;
        $site->visit = $visit;
        $site->traffic = $traffic;

        $site->save();

        return Redirect::to('/admin/sites');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site = Site::where('site_id', $id)->first();

        return view('admin.sites.edit',[
            'site' => $site,
            'janre_type' => Sites::$janre_type]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $site = Site::where('site_id', $id)->first();

        $site->site_id = $request->input('site_id');
        $site->title = $request->input('title');
        $site->visit = $request->input('visit');
        $site->traffic = $request->input('traffic');
        $site->janre = $site->janre = Sites::$janre_type[$site->janre];

        $site->save();
        return Redirect::to('/admin/sites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Site::destroy($id);
        return Redirect::to('/admin/sites');
    }
}
