<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SitesController;//шлях до контроллера
use App\Http\Controllers\AdminSitesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a groups which
| contains the "web" middleware groups. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return view('welcomes');
});
//Route::get('groups', 'SroupsController@groups');

Route::get('sites', [SitesController::class, 'index']);
Route::get('sites/{site_id}', [SitesController::class, 'site']);

Route::resource('/admin/sites', AdminSitesController::class);

