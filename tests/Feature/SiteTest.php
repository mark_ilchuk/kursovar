<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SiteTest extends TestCase
{

    public function testGetSiteTitle()
    {
        $site = new \App\Models\Sites();
        $site->setSiteTitle('Nike');
        $this->assertEquals($site->getSiteTitle(), 'Nike');
    }

    public function testSiteJanre()
    {
        $janre= new \App\Models\Sites();
        $janre->setJanre('мережі');
        $this->assertEquals($janre->getJanre(), 'мережі');
    }
}
